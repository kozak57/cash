<%@page contentType="text/html; charset=UTF-8" %>
<%@include file="../_header.jsp" %>

<div class="container">
    <form method="post" action="/user/deleteTransaction.html?id=${currentId}">
        <div class="container">

            Are you sure you want to delete transaction № ${currentId}?

            <button class="button btn-primary" type="submit">Yes</button>
            <a href="/user/viewTransaction.html">
                <button class="button btn-primary" type="button">NO</button>
            </a>
        </div>

    </form>


</div>

<%@include file="../../_footer.jsp" %>