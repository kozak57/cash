<c:if test="${not empty errors}">
    <div class="alert alert-warning " role="alert">
        <ul class="errors">
            <c:forEach var="error" items="${errors}">
            <li>${error}</li>
            </c:forEach>
    </div>
    </ul>
</c:if>


<form action="/user/${currentPage}" method="post">
    <div>
        <input type="hidden"
               class="input-group-lg col-lg-1"
               name="id"
               id="id"
               placeholder="id"
               value="${TransactionForm.id}">

        <input type="text"
               class="input-group-lg col-lg-3"
               name="name"
               id="name"
               placeholder="Name"
               value="${TransactionForm.name}">

        <input type="number"
               class="input-group-lg col-lg-3"
               max="2147483647"
               min="1"
               name="amount"
               id="amount"
               placeholder="Amount"
               value="${TransactionForm.amount}">

        <select class="input-group-lg col-lg-3"
                name="category">

            <c:forEach var="transactionCategory" items="${TransactionForm.categories}">
                <option>${transactionCategory}</option>
            </c:forEach>
        </select>

        <button class="button btn-primary" type="submit">Submit</button>

    </div>
</form>