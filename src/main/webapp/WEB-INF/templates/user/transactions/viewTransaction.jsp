<%@page contentType="text/html; charset=UTF-8" %>


<%@include file="../_header.jsp" %>

<div class="container">
    <h1>View transaction</h1>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th><a href="/user/viewTransaction.html?sortBy=id&order=${order}">Id</a></th>
            <th><a href="/user/viewTransaction.html?sortBy=name&order=${order}">Name</a></th>
            <th><a href="/user/viewTransaction.html?sortBy=amount&order=${order}">Amount</a></th>
            <th><a href="/user/viewTransaction.html?sortBy=category&order=${order}">Category</a></th>
            <th><a>Edit</a></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="transaction" items="${transactionSet}">
            <tr>
                <td> ${transaction.id} </td>
                <td> ${transaction.name} </td>
                <td> ${transaction.amount} </td>
                <td> ${transaction.category} </td>
                <td><a href="/user/editTransaction.html?id=${transaction.id}">Edit</a>/<a
                        href="/user/deleteTransaction.html?id=${transaction.id}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<%@include file="../../_footer.jsp" %>