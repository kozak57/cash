<%@page contentType="text/html; charset=UTF-8" %>
<%@include file="../_header.jsp" %>

<div class="container">
    <h1>Edit transaction number ${currentId} </h1>

    <%@include file="addAndEditForm.jsp" %>

</div>

    <%@include file="../../_footer.jsp" %>