<%@page contentType="text/html; charset=UTF-8" %>
<%@include file="../_header.jsp" %>


<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Operations with transactions
        </div>
        <div class="panel-body">
            <div class="btn-group" role="group">
                <a href="/user/addTransaction.html"><button type="button" class="btn btn-link">Add transaction</button></a>
                <a href="/user/viewTransaction.html?sortBy=id&order=asc"><button type="button" class="btn btn-link">View transactions</button></a>
                <a href="/user/enterId.html">
                    <button type="button" class="btn btn-link">Edit transaction</button>
                </a>
            </div>
        </div>
    </div>

</div>
<div class="main_content">