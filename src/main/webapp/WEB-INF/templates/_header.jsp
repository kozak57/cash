<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Project Cash</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/common.css">
    <script src="/js/jquery-2.1.3.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/index.html">Cash</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <c:if test="${!isLoggedIn}">
                    <li><a href="/registration.html">Registration</a></li>
                    <li><a href="/login.html">Log In</a></li>
                </c:if>
                <c:if test="${isLoggedIn}">
                    <li><a href="/user/home.html">Home</a></li>
                    <li><a href="/logout.html">Logout</a></li>
                </c:if>
                <li><a href="/about.html">About</a></li>

            </ul>
        </div>
    </div>
</nav>

<div class="main_content">
