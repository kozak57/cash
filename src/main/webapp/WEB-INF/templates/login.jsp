<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="_header.jsp" %>

<div class="container">

    <h1>Login page</h1>


    <c:if test="${not empty errors}">
        <ul class="errors">
            <c:forEach var="error" items="${errors}">
                <li>${error}</li>
            </c:forEach>
        </ul>
    </c:if>

    <form action="login.html" method="POST">

        <div class="input-group">
            <input type="text" placeholder="Login" name="login" id="login" value="${loginForm.login}">
        </div>
        <div class="input-group">
            <input type="text" placeholder="Password" name="password" id="password" value="">
        </div>
        <button class="button btn-primary" type="submit">Login</button>


    </form>
</div>

<%@include file="_footer.jsp" %>

