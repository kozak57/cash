<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="_header.jsp" %>

<div class="container">
    <h1>Registration page</h1>

    <c:if test="${not empty errors}">
        <ul class="errors">
            <c:forEach var="error" items="${errors}">
                <li>${error}</li>
            </c:forEach>
        </ul>
    </c:if>


    <form action="registration.html" method="POST">
        <div class="input-group">
            <input type="text" placeholder="Login" name="login" id="login" value="${registrationForm.login}">
        </div>
        <div class="input-group">
            <input type="text" placeholder="Username" name="name" id="name" value="${registrationForm.name}">
        </div>
        <div class="input-group">
            <input type="email" placeholder="Email" name="email" id="email" value="${registrationForm.email}">
        </div>
        <div class="input-group">
            <input type="password" placeholder="Password" name="password" id="password" value="">
        </div>
        <button type="submit" class="button btn-primary">Register</button>

    </form>
</div>

<%@include file="_footer.jsp" %>