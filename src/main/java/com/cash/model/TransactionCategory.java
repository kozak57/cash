package com.cash.model;

public enum TransactionCategory {
    JOB, FOOD, ENTERTAINMENT, FAMILY, OTHER
}
