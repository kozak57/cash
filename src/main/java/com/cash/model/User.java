package com.cash.model;

import com.cash.controller.security.PasswordEncoder;

public class User {
    private String login;
    private String passwordDigest;
    private String name;
    private String email;


    public User(String login, String password, String name, String email) {
        this.login = login;
        this.passwordDigest = PasswordEncoder.encode(password);
        this.name = name;
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public void setPasswordDigest(String password) {
        this.passwordDigest = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}