package com.cash.model;

public class Transaction {

    private String name;
    private int amount;
    private String category;
    private int id;
    private String userLogin;

    public Transaction(String name, int amount, String category, String userLogin) {
        this.name = name;
        this.amount = amount;
        this.userLogin = userLogin;
        this.category = category;
    }

    public Transaction(String name, int amount, String category, String userLogin, int id) {
        this(name, amount, category, userLogin);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}
