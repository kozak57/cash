package com.cash.dao;

import com.cash.model.User;

import java.sql.*;

public class UserDaoDbImpl implements UserDao {



    @Override
    public void create(User user) {

        String query = "INSERT INTO users (login, name, email,  passwordDigest) VALUES" +
                " (?,?,?,?)";
        System.out.println(query);
        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getName());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getPasswordDigest());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserByLogin(String login) {
        return find("SELECT * FROM users WHERE login = ?", login);

    }

    @Override
    public User getUserByLoginAndPassword(String login, String passwordDigest) {
        return find("SELECT * FROM users WHERE login = ? AND passwordDigest = ?", login, passwordDigest);
    }


    private User find(String selectQuery, Object... parameters) {
        System.out.println(selectQuery);

        User user = null;
        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(selectQuery);
            for (int i = 0; i < parameters.length; i++) {
                stmt.setString(i + 1, (String) parameters[i]);
            }

            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                user = new User(resultSet.getString("login"),
                        resultSet.getString("passwordDigest"),
                        resultSet.getString("name"),
                        resultSet.getString("email"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}
