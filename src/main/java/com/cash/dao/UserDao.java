package com.cash.dao;

import com.cash.model.User;

public interface UserDao {
    void create(User user);

    User getUserByLogin(String login);

    User getUserByLoginAndPassword(String login, String password);
}
