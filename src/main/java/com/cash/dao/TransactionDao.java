package com.cash.dao;

import com.cash.model.Transaction;

import java.util.List;

public interface TransactionDao {
    public void add(Transaction transaction);

    public Transaction getTransactionById(int id);

    public List<Transaction> getTransactionListByLogin(String login, String sortBy, String order);
    public void update(Transaction transaction);
    public void delete(int transactionID);


}
