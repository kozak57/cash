package com.cash.dao;

import com.cash.model.Transaction;

import java.sql.*;
import java.util.*;

public class TransactionDaoDbImpl implements TransactionDao {

    @Override
    public void add(Transaction transaction) {
        String query = "INSERT INTO transactions (name, amount, owner, category) VALUES " +
                "(?,?,?,?)";
        System.out.println(query);
        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, transaction.getName());
            stmt.setString(2, String.valueOf(transaction.getAmount()));
            stmt.setString(3, transaction.getUserLogin());
            stmt.setString(4, String.valueOf(transaction.getCategory()));
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Transaction> getTransactionListByLogin(String login, String sortBy, String order) {
        if (sortBy == null) sortBy = "id";

        String query = "SELECT * FROM transactions WHERE owner = ? ORDER BY ";
        query += sortBy + " " + order;
        return get(query, login);
    }

    @Override
    public void update(Transaction transaction) {
        String query = "UPDATE transactions SET name = ?, amount = ?, category = ? WHERE id = ?;";
        System.out.println(query);

        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, transaction.getName());
            stmt.setString(2, String.valueOf(transaction.getAmount()));
            stmt.setString(3, transaction.getCategory());
            stmt.setString(4, String.valueOf(transaction.getId()));
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Transaction getTransactionById(int id) {
        Transaction transaction = null;
        List<Transaction> transactions = get("SELECT * FROM transactions WHERE id = ?", id);
        if (transactions.size() > 0) {
            transaction = transactions.get(0);
        }
        return transaction;
    }

    @Override
    public void delete(int transactionId) {
        String query = "DELETE FROM transactions WHERE id = ?";
        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setInt(1, transactionId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private List<Transaction> get(String query, Object... params) {
        System.out.println(query);
        List<Transaction> transactions = new ArrayList<>();

        try (Connection connection = DbConnector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                stmt.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                Transaction transaction = new Transaction(
                        resultSet.getString("name"),
                        resultSet.getInt("amount"),
                        resultSet.getString("category"),
                        resultSet.getString("owner"),
                        resultSet.getInt("id")
                );
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return transactions;
    }

}
