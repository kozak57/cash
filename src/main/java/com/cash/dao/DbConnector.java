package com.cash.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Alexey on 04.02.2015.
 */
public class DbConnector {

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
        }

    }

    public static Connection getConnection() throws SQLException {
        String env = System.getenv("USERPROFILE");

        return DriverManager.getConnection("jdbc:mysql://localhost:3306/cash", "root", "sa");
    }
}
