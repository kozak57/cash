package com.cash.controller.registration;

import com.cash.util.BindUtil;
import com.cash.util.ValidationUtil;
import com.cash.controller.security.LoginService;
import com.cash.dao.UserDao;
import com.cash.dao.UserDaoDbImpl;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(value = "/registration.html")
public class RegistrationHTMLServlet extends HttpServlet {
    private UserDao userDao;
    private LoginService loginService;

    public RegistrationHTMLServlet() {
        userDao = new UserDaoDbImpl();
        loginService = new LoginService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RegistrationForm registrationForm = BindUtil.bind(req, RegistrationForm.class);
        List<String> errors = validate(registrationForm);

        if (errors.isEmpty()) {
            User user = new User(
                    registrationForm.getLogin(),
                    registrationForm.getPassword(),
                    registrationForm.getName(),
                    registrationForm.getEmail());
            userDao.create(user);
            loginService.login(req, user);
            req.getSession().setAttribute("currentUser", user);
            resp.sendRedirect("registrationSuccess.html");
        } else {
            req.setAttribute("registrationForm", registrationForm);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/registration.jsp").forward(req, resp);
        }
    }

    private List<String> validate(RegistrationForm registrationForm) {
        List<String> errors = ValidationUtil.validate(registrationForm);

        if (userDao.getUserByLogin(registrationForm.getLogin()) != null) {
            errors.add("Current login is used");
        }

        return errors;
    }
}
