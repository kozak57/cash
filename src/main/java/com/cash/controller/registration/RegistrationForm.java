package com.cash.controller.registration;


import com.cash.util.NotEmpty;

public class RegistrationForm {
    @NotEmpty("Login field cannot be blank.")
    private String login;
    @NotEmpty("Username field cannot be blank.")
    private String name;
    @NotEmpty("Email fiel cannot be blank")
    private String email;
    @NotEmpty("Password field cannot be blank.")
    private String password;


    public RegistrationForm(String login, String name, String email, String password) {
        this.login = login.trim();
        this.name = name.trim();
        this.email = email.trim();
        this.password = password;
    }

    public RegistrationForm() {
        this("", "", "", "");
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
