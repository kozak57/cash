package com.cash.controller.security;


import com.cash.model.User;

import javax.servlet.http.HttpServletRequest;

public class LoginService {

    private static final String LOGGED_IN_USERNAME_KEY = "LoggedInUsername";

    public void login(HttpServletRequest req, User user) {
        req.getSession().setAttribute(LOGGED_IN_USERNAME_KEY, user.getLogin());
    }

    public boolean isLoggedIn(HttpServletRequest req) {
        return req.getSession().getAttribute(LOGGED_IN_USERNAME_KEY) != null;
    }

    public String getCurrentUsername(HttpServletRequest req) {
        return (String) req.getSession().getAttribute(LOGGED_IN_USERNAME_KEY);
    }

    public void logout(HttpServletRequest req) {
        req.getSession().invalidate();
    }
}
