package com.cash.controller.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncoder {
    private static MessageDigest digest;
    private static final String PASSWORD_SALT = "8q7RFasdasdGVHN";
    static {
        try {
            digest = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public static String encode(String password){
        try {
            return new String(digest.digest((password + PASSWORD_SALT).getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Password hashing error", e);
        }
    }
}
