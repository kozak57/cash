package com.cash.controller.security;

import com.cash.dao.UserDao;
import com.cash.dao.UserDaoDbImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@WebFilter(urlPatterns = "/*")
public class CurrentUserFilter implements Filter {

    private UserDao userDao;
    private LoginService loginService;

    public CurrentUserFilter() {
        userDao = new UserDaoDbImpl();
        loginService = new LoginService();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        httpRequest.setAttribute("isLoggedIn", loginService.isLoggedIn(httpRequest));
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
