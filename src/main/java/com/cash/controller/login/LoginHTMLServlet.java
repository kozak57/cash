package com.cash.controller.login;

import com.cash.util.BindUtil;
import com.cash.controller.security.LoginService;
import com.cash.controller.security.PasswordEncoder;
import com.cash.dao.UserDao;
import com.cash.dao.UserDaoDbImpl;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(value = "/login.html")
public class LoginHTMLServlet extends HttpServlet {
    private LoginService loginService;
    private UserDao userDao;

    public LoginHTMLServlet() {
        userDao = new UserDaoDbImpl();
        loginService = new LoginService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginForm loginForm = BindUtil.bind(req, LoginForm.class);

        List<String> errors = validate(loginForm);

        if (errors.isEmpty()) {
            User user = userDao.getUserByLoginAndPassword(loginForm.getLogin(),
                    PasswordEncoder.encode(loginForm.getPassword()));
            if (user != null) {
                loginService.login(req, user);
            } else {
                errors.add("User name or password is wrong");
            }
        }
        if (errors.isEmpty()) {
            req.getSession().setAttribute("currentUser", userDao.getUserByLogin(loginService.getCurrentUsername(req)));
            resp.sendRedirect("user/home.html");
        } else {
            req.setAttribute("errors", errors);
            req.setAttribute("loginForm", loginForm);
            req.getRequestDispatcher("/WEB-INF/templates/login.jsp").forward(req, resp);
        }
    }

    private List<String> validate(LoginForm loginForm) {
        List<String> errors = new ArrayList<>();

        if (loginForm.getLogin().isEmpty()) {
            errors.add("Login is empty");
        }
        if (loginForm.getPassword().isEmpty()) {
            errors.add("Password is empty");
        }
        return errors;
    }
}


