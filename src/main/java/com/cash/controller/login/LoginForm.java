package com.cash.controller.login;


public class LoginForm {
    private final String login;
    private final String password;

    public LoginForm() {
        this("", "");
    }

    public LoginForm(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}