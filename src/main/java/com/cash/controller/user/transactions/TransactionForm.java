package com.cash.controller.user.transactions;

import com.cash.util.NotEmpty;

import java.util.Arrays;
import java.util.List;

public class TransactionForm {

    @NotEmpty("Name cannot be empty")
    private String name;
    @NotEmpty("Amount cannot be empty")
    private Integer amount;
    @NotEmpty("Please select category")
    private String category;
    private Integer id;

    private List<String> categories = Arrays.asList("JOB", "FOOD", "ENTERTAINMENT", "FAMILY", "OTHER");


    public TransactionForm(String name, Integer amount, String category) {
        this.name = name;
        this.amount = amount;
        this.category = category;
    }

    public TransactionForm(String name, Integer amount, String category, int id) {
        this(name, amount, category);
        this.id = id;
    }

    public TransactionForm() {
        this("", null, "");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getCategories() {
        return categories;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
