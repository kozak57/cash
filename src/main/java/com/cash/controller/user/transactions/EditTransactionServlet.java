package com.cash.controller.user.transactions;

import com.cash.util.BindUtil;
import com.cash.util.ValidationUtil;
import com.cash.dao.TransactionDao;
import com.cash.dao.TransactionDaoDbImpl;
import com.cash.dao.UserDao;
import com.cash.dao.UserDaoDbImpl;
import com.cash.model.Transaction;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user/editTransaction.html")
public class EditTransactionServlet extends HttpServlet {

    private TransactionDao transactionDao;
    private UserDao userDao;

    public EditTransactionServlet() {
        transactionDao = new TransactionDaoDbImpl();
        userDao = new UserDaoDbImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("currentPage", "editTransaction.html");

        int id = Integer.parseInt(req.getParameter("id"));
        req.setAttribute("currentId", id);

        Transaction transaction = transactionDao.getTransactionById(id);
        User user = (User) req.getSession().getAttribute("currentUser");
        if (!transaction.getUserLogin().equals(user.getLogin())) {
            req.getRequestDispatcher("/WEB-INF/templates/errorPage.jsp").forward(req, resp);
        } else {

            TransactionForm transactionForm = new TransactionForm(
                    transaction.getName(),
                    transaction.getAmount(),
                    transaction.getCategory(),
                    id);

            req.setAttribute("TransactionForm", transactionForm);
            req.getRequestDispatcher("/WEB-INF/templates/user/transactions/editTransaction.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        TransactionForm transactionForm = BindUtil.bind(req, TransactionForm.class);
        List<String> errors = ValidationUtil.validate(transactionForm);

        Transaction transactionById = transactionDao.getTransactionById(transactionForm.getId());
        User user = (User) req.getSession().getAttribute("currentUser");
        if (!transactionById.getUserLogin().equals(user.getLogin())) {
            req.getRequestDispatcher("/WEB-INF/templates/errorPage.jsp").forward(req, resp);
        } else {
            if (errors.isEmpty()) {
                Transaction transaction = new Transaction(
                        transactionForm.getName(),
                        transactionForm.getAmount(),
                        transactionForm.getCategory(),
                        user.getLogin(),
                        transactionForm.getId()
                );
                transactionDao.update(transaction);
                resp.sendRedirect("/user/viewTransaction.html");
            } else {
                req.setAttribute("TransactionForm", transactionForm);
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("/WEB-INF/templates/user/transactions/editTransaction.jsp").forward(req, resp);
            }
        }
    }
}
