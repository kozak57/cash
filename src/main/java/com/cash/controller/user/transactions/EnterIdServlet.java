package com.cash.controller.user.transactions;

import com.cash.dao.TransactionDao;
import com.cash.dao.TransactionDaoDbImpl;
import com.cash.model.Transaction;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user/enterId.html")
public class EnterIdServlet extends HttpServlet {
    private TransactionDao transactionDao;

    public EnterIdServlet() {
        transactionDao = new TransactionDaoDbImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/user/transactions/enterId.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        if (req.getParameterNames().hasMoreElements() && !req.getParameter("id").isEmpty()) {

            int id = Integer.parseInt(req.getParameter("id"));

            Transaction transactionById = transactionDao.getTransactionById(id);
            User user = (User) req.getSession().getAttribute("currentUser");
            if (!transactionById.getUserLogin().equals(user.getLogin())) {
                req.getRequestDispatcher("/WEB-INF/templates/errorPage.jsp").forward(req, resp);
            } else
                resp.sendRedirect("/user/editTransaction.html?id=" + id);
        } else req.getRequestDispatcher("/WEB-INF/templates/user/transactions/enterId.jsp").forward(req, resp);

    }
}
