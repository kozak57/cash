package com.cash.controller.user.transactions;

import com.cash.util.BindUtil;
import com.cash.util.ValidationUtil;
import com.cash.dao.TransactionDao;
import com.cash.dao.TransactionDaoDbImpl;
import com.cash.model.Transaction;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = "/user/addTransaction.html")
public class AddTransactionServlet extends HttpServlet {
    private TransactionDao transactionDao;
    private TransactionForm transactionForm;

    public AddTransactionServlet() {
        transactionDao = new TransactionDaoDbImpl();
        transactionForm = new TransactionForm();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("currentPage", "addTransaction.html");
        req.setAttribute("TransactionForm", transactionForm);
        req.getRequestDispatcher("/WEB-INF/templates/user/transactions/addTransaction.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TransactionForm transactionForm = BindUtil.bind(req,  TransactionForm.class);
        List<String> errors = ValidationUtil.validate(transactionForm);

        if (errors.isEmpty()) {
            User user = (User) req.getSession().getAttribute("currentUser");
            Transaction transaction = new Transaction(transactionForm.getName(), transactionForm.getAmount(), transactionForm.getCategory(), user.getLogin());
            transactionDao.add(transaction);
            resp.sendRedirect("/user/viewTransaction.html");
        } else {
            req.setAttribute("TransactionForm", transactionForm);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/templates/user/transactions/addTransaction.jsp").forward(req, resp);
        }
    }
}


