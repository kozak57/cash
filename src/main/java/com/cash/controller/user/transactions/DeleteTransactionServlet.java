package com.cash.controller.user.transactions;

import com.cash.dao.TransactionDao;
import com.cash.dao.TransactionDaoDbImpl;
import com.cash.model.Transaction;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/user/deleteTransaction.html")
public class DeleteTransactionServlet extends HttpServlet {
    private TransactionDao transactionDao;

    public DeleteTransactionServlet() {
        transactionDao = new TransactionDaoDbImpl();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id = Integer.parseInt(req.getParameter("id"));
        req.setAttribute("currentId", id);
        req.getRequestDispatcher("/WEB-INF/templates/user/transactions/deleteTransaction.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        req.setAttribute("currentId", id);

        Transaction transaction = transactionDao.getTransactionById(id);
        User user = (User) req.getSession().getAttribute("currentUser");
        if (!transaction.getUserLogin().equals(user.getLogin())) {
            req.getRequestDispatcher("/WEB-INF/templates/errorPage.jsp").forward(req, resp);
        } else {
            transactionDao.delete(id);
            resp.sendRedirect("/user/viewTransaction.html");
        }
    }
}
