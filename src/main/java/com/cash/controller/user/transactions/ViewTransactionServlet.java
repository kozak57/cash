package com.cash.controller.user.transactions;

import com.cash.dao.TransactionDao;
import com.cash.dao.TransactionDaoDbImpl;
import com.cash.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/user/viewTransaction.html")
public class ViewTransactionServlet extends HttpServlet {

    private TransactionDao transactionDao;

    public ViewTransactionServlet() {
        transactionDao = new TransactionDaoDbImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("currentUser");


        String sortBy = req.getParameter("sortBy");
        String currentOrder = req.getParameter("order");
        String order = "ASC";
        if (currentOrder !=null && currentOrder.equals("ASC")) {
            order = "DESC";
        } else {
            if (currentOrder !=null && currentOrder.equals("DESC")) {
                order = "ASC";
            }
        }

        req.setAttribute("order", order);

        System.out.println(sortBy);
        req.setAttribute("transactionSet", transactionDao.getTransactionListByLogin(user.getLogin(), sortBy, order));
        req.getRequestDispatcher("/WEB-INF/templates/user/transactions/viewTransaction.jsp").forward(req, resp);
    }
}
