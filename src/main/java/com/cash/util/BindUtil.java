package com.cash.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.List;

public class BindUtil {
    public static <T> T bind(HttpServletRequest req, Class<T> clazz) throws ServletException {

        List<Field> privateFields = ClassUtil.getPrivateFields(clazz);
        T form = null;
        try {
            form = clazz.newInstance();
            for (Field field : privateFields) {
                field.setAccessible(true);
                if (field.getType() == String.class) {
                    field.set(form, req.getParameter(field.getName()));
                }

                if (field.getType() == Integer.class) {
                    if (req.getParameter(field.getName()).isEmpty()) {
                        field.set(form, 0);
                    } else field.set(form, Integer.parseInt(req.getParameter(field.getName())));
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }

        return form;
    }
}
