CREATE TABLE users (
  login VARCHAR(255) PRIMARY KEY,
  name  VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  passwordDigest VARCHAR(255) NOT NULL
);

CREATE TABLE transactions (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name     VARCHAR(255) NOT NULL,
  amount INT NOT NULL,
  owner    VARCHAR(255) REFERENCES users (login),
  category VARCHAR(255)
);

